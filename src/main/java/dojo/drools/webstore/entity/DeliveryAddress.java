package dojo.drools.webstore.entity;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class DeliveryAddress {

    private String postalCode;
    private City city;
    private String addressLine;

}
