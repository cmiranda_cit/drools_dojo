package dojo.drools.webstore.entity;

public enum PaymentMethod {
    CASH, DEBIT, CREDIT, VOUCHER
}