package dojo.drools.webstore.entity;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class CartAmount {

    @Getter
    private double itensPrice = 0;

    @Setter
    @Getter
    private double discountPrice = 0;

    @Setter
    @Getter
    private double freightPrice = 0;

    public CartAmount(List<Item> items) {
        items.forEach((i) -> {
            itensPrice += i.getPriceAmount();
        });
    }

    public double getTotal() {
        return (itensPrice + freightPrice) - discountPrice;
    }

}
