package dojo.drools.webstore.entity;

import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Cart {

    private List<Item> items;
    private Purchase purchase;
    private DeliveryAddress delivery;
    private CartAmount amount;
    private List<String> vouchers;

    public int getItemsQuantity() {
        return items.stream().mapToInt(i -> i.getQuantity()).sum();
    }

}
