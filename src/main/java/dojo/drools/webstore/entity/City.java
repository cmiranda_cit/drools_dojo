package dojo.drools.webstore.entity;

public enum City {
    CAMPINAS, SAO_PAULO, BARUERI, ROMA, NY, PARIS
}