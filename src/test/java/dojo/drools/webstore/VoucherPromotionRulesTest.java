package dojo.drools.webstore;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import dojo.drools.webstore.entity.Cart;
import dojo.drools.webstore.entity.DeliveryAddress;
import dojo.drools.webstore.entity.Item;
import dojo.drools.webstore.entity.PaymentMethod;
import dojo.drools.webstore.entity.ProductType;
import dojo.drools.webstore.entity.Purchase;
import dojo.drools.webstore.services.RuleManager;

public class VoucherPromotionRulesTest {

    @Test
    /** CHALLENGE 3 */
    public void cartWithOneWatchAndVoucherWATCHES10OFFThenShouldGivePercentualDiscountByItem() {

        // Given I have a freight calculate rules
        RuleManager ruleManager = new RuleManager();
        ruleManager.addRule("freight/calculate_freight_by_postalcode.drl");
        // .. and shipping promotion rules
        // ruleManager.addRule("promotion/shipping_discount.drl");
        // ruleManager.addRule("promotion/voucher.drl");

        // ... check if compilation is ok!
        Assert.assertTrue(ruleManager.build());

        // And I have a cart with 1 items sku 101
        List<Item> items = new ArrayList<>();
        items.add(Item.builder().price(120.0).quantity(1).typeofItem(ProductType.WATCHES).build());
        DeliveryAddress deliveryAddress = DeliveryAddress.builder().postalCode("10160000").build();
        // .. with credit card payment
        Purchase purchase = Purchase.builder().paymentMethod(PaymentMethod.CREDIT).build();
        // .. and add promotional voucher
        Cart cart = Cart.builder().items(items).delivery(deliveryAddress).purchase(purchase)
                .vouchers(Arrays.asList("WATCHES10OFF")).build();

        // When I call rule engine
        ruleManager.statelessExecute(cart);

        // Then Freight price should be 10$
        Assert.assertNotNull(cart.getAmount());
        Assert.assertEquals(10.0D, cart.getAmount().getFreightPrice(), 0.0);
        // ... and discount should be $12 (by freight rules)
        Assert.assertEquals(12.0D, cart.getAmount().getDiscountPrice(), 0.0);
        Assert.assertEquals(118.0D, cart.getAmount().getTotal(), 0.0);

    }

    @Test
    /** CHALLENGE 3 */
    public void cartWithTwoWatchAndAnotherAndVoucherWATCHES10OFFThenShouldGivePercentualDiscountByItem() {

        // Given I have a freight calculate rules
        RuleManager ruleManager = new RuleManager();
        ruleManager.addRule("freight/calculate_freight_by_postalcode.drl");
        // .. and shipping promotion rules
        // ruleManager.addRule("promotion/shipping_discount.drl");
        // ruleManager.addRule("promotion/voucher.drl");

        // ... check if compilation is ok!
        Assert.assertTrue(ruleManager.build());

        // And I have a cart with 1 items sku 101
        List<Item> items = new ArrayList<>();
        items.add(Item.builder().price(200.0).quantity(2).typeofItem(ProductType.WATCHES).build());
        items.add(Item.builder().price(100.0).quantity(1).typeofItem(ProductType.MEDICINES).build());
        DeliveryAddress deliveryAddress = DeliveryAddress.builder().postalCode("13160001").build();
        // .. with credit card payment
        Purchase purchase = Purchase.builder().paymentMethod(PaymentMethod.CREDIT).build();
        // .. and add promotional voucher
        Cart cart = Cart.builder().items(items).delivery(deliveryAddress).purchase(purchase)
                .vouchers(Arrays.asList("WATCHES10OFF")).build();

        // When I call rule engine
        ruleManager.statelessExecute(cart);

        // Then Freight price should be 10$
        Assert.assertNotNull(cart.getAmount());
        Assert.assertEquals(0.0D, cart.getAmount().getFreightPrice(), 10.0);
        // ... and discount should be $40 (by voucher rules)
        Assert.assertEquals(40.0D, cart.getAmount().getDiscountPrice(), 0.0);
        Assert.assertEquals(285.0D, cart.getAmount().getTotal(), 0.0);

    }

}
