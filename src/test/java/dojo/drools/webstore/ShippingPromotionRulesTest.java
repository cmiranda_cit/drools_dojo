package dojo.drools.webstore;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import dojo.drools.webstore.entity.Cart;
import dojo.drools.webstore.entity.DeliveryAddress;
import dojo.drools.webstore.entity.Item;
import dojo.drools.webstore.entity.PaymentMethod;
import dojo.drools.webstore.entity.Purchase;
import dojo.drools.webstore.services.RuleManager;

public class ShippingPromotionRulesTest {

    @Test
    /** CHALLENGE 2 */
    public void cartWithFourItemsSku1010PaidByCreditCardThenFreightShouldBeZeroByRule() {

        // Given I have a freight calculate rules
        RuleManager ruleManager = new RuleManager();
        ruleManager.addRule("freight/calculate_freight_by_postalcode.drl");
        // .. and shipping promotion rules
        // ruleManager.addRule("promotion/shipping_discount.drl");

        // ... check if compilation is ok!
        Assert.assertTrue(ruleManager.build());

        // And I have a cart with 4 items sku 101
        List<Item> items = new ArrayList<>();
        items.add(Item.builder().price(1.0).quantity(4).sku(1010L).build());
        DeliveryAddress deliveryAddress = DeliveryAddress.builder().postalCode("10160000").build();
        // .. with credit card payment
        Purchase purchase = Purchase.builder().paymentMethod(PaymentMethod.CREDIT).build();
        Cart cart = Cart.builder().items(items).delivery(deliveryAddress).purchase(purchase).build();

        // When I call rule engine
        ruleManager.statelessExecute(cart);

        // Then Freight price should be 0$
        Assert.assertNotNull(cart.getAmount());
        Assert.assertEquals(0.0D, cart.getAmount().getFreightPrice(), 0.0);
        // ... and discount should be $10 (by freight rules)
        Assert.assertEquals(10.0D, cart.getAmount().getDiscountPrice(), 0.0);

    }

}
