package dojo.drools.webstore.entity;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Item {

    private long sku;
    private double price;
    private int quantity;
    private ProductType typeofItem;

    public double getPriceAmount() {
        return price * quantity;
    }

}
