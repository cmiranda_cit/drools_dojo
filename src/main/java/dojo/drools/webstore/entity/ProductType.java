package dojo.drools.webstore.entity;

public enum ProductType {
    GROCERIES, MEDICINES, WATCHES, LUXURYGOODS
}