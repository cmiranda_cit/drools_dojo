package dojo.drools.webstore.entity;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Purchase {

    private PaymentMethod paymentMethod;
    private Double discount;

}
