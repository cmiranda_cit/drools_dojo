package dojo.drools.webstore;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import dojo.drools.webstore.entity.Cart;
import dojo.drools.webstore.entity.DeliveryAddress;
import dojo.drools.webstore.entity.Item;
import dojo.drools.webstore.services.RuleManager;

public class FreightPriceRulesTest {

    @Test
    /** CHALLENGE 1 */
    public void cartWithOneItemAndPostalCodeNotStartsWith13ThenFreightShouldBe10ByRule() {

        // Given I have a freight calculate rules
        RuleManager ruleManager = new RuleManager();
        ruleManager.addRule("freight/calculate_freight_by_postalcode.drl");

        // ... check if compilation is ok!
        Assert.assertTrue(ruleManager.build());

        // And I have a cart with one item and postal code 13*
        List<Item> items = new ArrayList<>();
        items.add(Item.builder().price(1.0).quantity(1).build());
        DeliveryAddress deliveryAddress = DeliveryAddress.builder().postalCode("10160000").build();
        Cart cart = Cart.builder().items(items).delivery(deliveryAddress).build();

        // ... check if not exists amounts (then not exists freight price)
        Assert.assertNull(cart.getAmount());

        // When I call rule engine
        ruleManager.statelessExecute(cart);

        // Then Freight price should be 10$
        Assert.assertNotNull(cart.getAmount());
        Assert.assertEquals(10.0D, cart.getAmount().getFreightPrice(), 0.0);

    }
    
    @Test
    /** CHALLENGE 1 */
    public void cartWithThirtyFiveItemAndPostalCodeNotStartsWith13ThenFreightShouldBe30ByRule() {

        // Given I have a freight calculate rules
        RuleManager ruleManager = new RuleManager();
        ruleManager.addRule("freight/calculate_freight_by_postalcode.drl");

        // ... check if compilation is ok!
        Assert.assertTrue(ruleManager.build());

        // And I have a cart with one item and postal code 13*
        List<Item> items = new ArrayList<>();
        items.add(Item.builder().price(1.0).quantity(35).build());
        DeliveryAddress deliveryAddress = DeliveryAddress.builder().postalCode("10160000").build();
        Cart cart = Cart.builder().items(items).delivery(deliveryAddress).build();

        // ... check if not exists amounts (then not exists freight price)
        Assert.assertNull(cart.getAmount());

        // When I call rule engine
        ruleManager.statelessExecute(cart);

        // Then Freight price should be 30$
        Assert.assertNotNull(cart.getAmount());
        Assert.assertEquals(30.0D, cart.getAmount().getFreightPrice(), 0.0);

    }

    @Test
    /** CHALLENGE 1 */
    public void cartWithOneItemAndPostalCodeStartsWith13ThenFreightShouldBe25ByRule() {

        // Given I have a freight calculate rules
        RuleManager ruleManager = new RuleManager();
        ruleManager.addRule("freight/calculate_freight_by_postalcode.drl");

        // ... check if compilation is ok!
        Assert.assertTrue(ruleManager.build());

        // And I have a cart with one item and postal code 13*
        List<Item> items = new ArrayList<>();
        items.add(Item.builder().price(1.0).quantity(1).build());
        DeliveryAddress deliveryAddress = DeliveryAddress.builder().postalCode("13160000").build();
        Cart cart = Cart.builder().items(items).delivery(deliveryAddress).build();

        // ... check if not exists amounts (then not exists freight price)
        Assert.assertNull(cart.getAmount());

        // When I call rule engine
        ruleManager.statelessExecute(cart);

        // Then Freight price should be 25$
        Assert.assertNotNull(cart.getAmount());
        Assert.assertEquals(25.0D, cart.getAmount().getFreightPrice(), 0.0);

    }

    @Test
    /** CHALLENGE 1 */
    public void cartWithTweentyItemAndPostalCodeStartsWith13ThenFreightShouldBe50ByRule() {

        // Given I have a freight calculate rules
        RuleManager ruleManager = new RuleManager();
        ruleManager.addRule("freight/calculate_freight_by_postalcode.drl");

        // ... check if compilation is ok!
        Assert.assertTrue(ruleManager.build());

        // And I have a cart with one item and postal code 13*
        List<Item> items = new ArrayList<>();
        items.add(Item.builder().price(1.0).quantity(20).build());
        DeliveryAddress deliveryAddress = DeliveryAddress.builder().postalCode("13160000").build();
        Cart cart = Cart.builder().items(items).delivery(deliveryAddress).build();

        // ... check if not exists amounts (then not exists freight price)
        Assert.assertNull(cart.getAmount());

        // When I call rule engine
        ruleManager.statelessExecute(cart);

        // Then Freight price should be 50$
        Assert.assertNotNull(cart.getAmount());
        Assert.assertEquals(50.0D, cart.getAmount().getFreightPrice(), 0.0);

    }

}
