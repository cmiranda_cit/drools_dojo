package dojo.drools.webstore.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.drools.core.io.impl.ResourceFactoryServiceImpl;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.KieRepository;
import org.kie.api.builder.Message.Level;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.StatelessKieSession;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RuleManager {

    private List<String> rules = new ArrayList<String>();

    public RuleManager addRule(final String resource) {
        rules.add(resource);
        return this;
    }

    public boolean build() {

        // Service to manage business rules container and knowledges
        KieServices ks = KieServices.Factory.get();

        // Create the in-memory File System and add the resources files to it
        KieFileSystem kfs = ks.newKieFileSystem();

        // Create the builder for the resources of the File System
        KieBuilder kbuilder = ks.newKieBuilder(kfs);

        ResourceFactoryServiceImpl resourceFactory = new ResourceFactoryServiceImpl();
        for (String resource : rules) {
            kfs.write(resourceFactory.newClassPathResource(resource).setResourceType(ResourceType.DRL));
            // kfs.write(resource, getFileContent(resource));
        }

        // build all resources
        kbuilder.buildAll();

        // Check for errors
        if (kbuilder.getResults().hasMessages(Level.ERROR)) {
            log.error("Fail: {}", kbuilder.getResults());
            return false;
        }

        return true;

    }

    private KieContainer getKieContainer() {

        KieServices ks = KieServices.Factory.get();

        // Get repository
        KieRepository kr = ks.getRepository();

        // Create the Container, wrapping the KieModule with the given ReleaseId
        KieContainer container = ks.newKieContainer(kr.getDefaultReleaseId());

        return container;

    }

    public StatelessKieSession statelessExecute(Object... items) {

        // Configure and create the StatelessKieSession
        StatelessKieSession ksession = getKieContainer().newStatelessKieSession();

        // Set global
        ksession.setGlobal("logger", log);

        // execute all
        ksession.execute(Arrays.asList(items));

        return ksession;
    }

    public KieSession newSession() {

        // Configure and create the StatelessKieSession
        KieSession ksession = getKieContainer().newKieSession();

        // Set global
        ksession.setGlobal("logger", log);

        return ksession;
    }

    public KieSession sessionExecute(KieSession session, Object... items) {

        if (session == null) {
            session = newSession();
        }

        for (Object item : items) {
            session.insert(item);
        }

        session.fireAllRules();
        return session;
    }

    public KieSession execute(Object... items) {
        return sessionExecute(null, items);
    }

}
